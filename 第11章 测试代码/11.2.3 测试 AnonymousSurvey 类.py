import unittest
from urllib import response
from survey import AnonymousSurvey

class TestAnonymousSurvey(unittest.TestCase):
    
    def test_store_single_response(self):
        """测试单个答案会被妥善地存储."""
        question = "What language did you first learn to speak?"
        my_survey = AnonymousSurvey(question)
        my_survey.responses = ['English']

        self.assertIn('English', my_survey.responses)

    def test_store_three_responses(self):
        """测试3个答案会被妥善地存储."""
        question = "What language did you first learn to speak?"
        my_survey = AnonymousSurvey(question)
        responses = ['English', 'Spanish', 'Mandarin']
        for response in responses:
            my_survey.store_response(response)

        # 断言
        for response in responses:
            self.assertIn(response, my_survey.responses)

if __name__ == '__main__':
    unittest.main()
