import unittest
from name_function_v1 import get_formatted_name

class NamesTestCase(unittest.TestCase): # 定义测试用例类，继承 unittest.TestCase

    def test_first_last_name(self): # 定义测试方法
        """特别注意：测试方法必须以 test_ 打头，否则不会被调用"""
        format_name = get_formatted_name('janis', 'joplin')
        self.assertEqual(format_name, 'Janis Joplin')  # 断言

if __name__ == '__main__': # 该文件作为主程序 执行时，特殊变量__name__被设置为：__main__
    unittest.main() # 运行测试用例


'''
.      (这个点句表明有一个测试通过了)
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
'''