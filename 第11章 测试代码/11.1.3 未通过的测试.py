import unittest
from name_function_v2 import get_formatted_name

class NamesTestCase(unittest.TestCase): # 定义测试用例类，继承 unittest.TestCase

    def test_first_last_name(self): # 定义测试方法
        format_name = get_formatted_name('janis', 'joplin')
        self.assertEqual(format_name, 'Janis Joplin')  # 断言

if __name__ == '__main__': # 该文件作为主程序 执行时，特殊变量__name__被设置为：__main__
    unittest.main() # 运行测试用例s


'''
E
======================================================================
ERROR: test_first_last_name (__main__.NamesTestCase)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "e:\Kzone\CodeLib\PythonCode\books\Python-Crash-Course\第11章 测试代码\11.1.3 未通过的测试.py", line 7, in test_first_last_name
    format_name = get_formatted_name('janis', 'joplin')
TypeError: get_formatted_name() missing 1 required positional argument: 'last'

----------------------------------------------------------------------
Ran 1 test in 0.001s

FAILED (errors=1)
'''