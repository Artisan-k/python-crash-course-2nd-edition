def get_formatted_name(first, last):
    fullname = f"{first} {last}"
    return fullname.title()