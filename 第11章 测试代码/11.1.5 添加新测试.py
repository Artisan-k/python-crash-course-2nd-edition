import unittest
from name_function_v3 import get_formatted_name

class NamesTestCase(unittest.TestCase): 

    def test_first_last_name(self): 
        format_name = get_formatted_name('janis', 'joplin')
        self.assertEqual(format_name, 'Janis Joplin')  

    def test_first_last_middle_name(self): # 定义新测试方法
        format_name = get_formatted_name('wolfgang', 'mozart', 'amadeus')
        # self.assertEqual(format_name, 'Wolfgang Mozart Amadeus') # 失败测试
        self.assertEqual(format_name, 'Wolfgang Amadeus Mozart') 


if __name__ == '__main__': 
    unittest.main() 


''' 如果不相等
F.
======================================================================
FAIL: test_first_last_middle_name (__main__.NamesTestCase)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "e:\Kzone\CodeLib\PythonCode\books\Python-Crash-Course\第11章 测试代码\11.1.5 添加新测试.py", line 12, in test_first_last_middle_name
    self.assertEqual(format_name, 'Wolfgang Mozart Amadeus')
AssertionError: 'Wolfgang Amadeus Mozart' != 'Wolfgang Mozart Amadeus'
- Wolfgang Amadeus Mozart
+ Wolfgang Mozart Amadeus


----------------------------------------------------------------------
Ran 2 tests in 0.001s

FAILED (failures=1)

'''

''' 如果都通过
.. (这里有两个点，表明有两个测试通过)
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
'''
