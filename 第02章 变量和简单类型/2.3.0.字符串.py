str = "this's a string" # 用双引号""的字符串
print(str)
str = 'this is also a "string"' # 用单引号的字符串
print(str)

'''
this's a string
this is also a "string"
'''