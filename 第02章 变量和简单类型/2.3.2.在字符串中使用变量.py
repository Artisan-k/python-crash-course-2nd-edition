first_name = "ada"
last_name = "lpvelace"
full_name = f"{first_name} {last_name}" # f字符串

print(full_name) # ada lpvelace
print(f"Hell0, {full_name.title()}") # Hell0, Ada Lpvelace

message = f"Hell0, {full_name.title()}"  
print(message) # Hell0, Ada Lpvelace

