
charts1 = ['A', 'B', 'C']
charts2 = []

def do(charts1, charts2):
    while charts1:
        charts2.append(charts1.pop())

do(charts1, charts2)

print(charts1) #charts1 被修改了
print(charts2) #charts2 被修改了


'''
[]
['C', 'B', 'A']
'''