
charts = ['A', 'B', 'C']

def do(*charts):
    for c in charts:
        print(c)

do(charts)
do('a', 'c')


'''
['A', 'B', 'C']
a
c
'''