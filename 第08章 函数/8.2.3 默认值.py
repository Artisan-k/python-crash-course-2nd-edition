def describe_pet(pet_name,animal_type='dog'):
    print(f"\nI have {animal_type}")
    print(f"My {animal_type}'s name is {pet_name}")

describe_pet("harry") 

'''
I have dog
My dog's name is harry
'''