
charts1 = ['A', 'B', 'C']
charts2 = []

def do(charts1, charts2):
    while charts1:
        charts2.append(charts1.pop())

do(charts1[:], charts2) # 切片表示法[:]创建列表的副本 

print(charts1) #修改的是列表的副本,所有 charts1 没被修改
print(charts2) #charts2 被修改了


'''
['A', 'B', 'C']
['C', 'B', 'A']
'''