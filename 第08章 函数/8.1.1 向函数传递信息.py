def greet_user(username):
    print(f"Hello, {username.title()}")
 
greet_user("kevin") # Hello, Kevin