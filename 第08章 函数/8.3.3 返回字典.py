def build_person(first_name, last_name, middle_name=''):
    if middle_name: # Python 将非空字符串解读为True
        person = f"{first_name} {middle_name} {last_name}"
    else:
       person = f"{first_name} {last_name}"
    return person

musician = build_person("jimi", 'hendrix')
print(musician) # jimi hendrix

musician = build_person("john",'hooker', 'lee')
print(musician) # john lee hooker