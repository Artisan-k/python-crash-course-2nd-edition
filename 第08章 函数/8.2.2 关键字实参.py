def describe_pet(animal_type, pet_name):
    print(f"\nI have {animal_type}")
    print(f"My {animal_type}'s name is {pet_name}")

describe_pet(pet_name="harry",animal_type='hamster') 

'''
I have hamster
My hamster's name is harry
'''