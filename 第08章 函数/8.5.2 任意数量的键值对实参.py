def buil_profile(first, last, **user_info): # **的形参，用于收集任意数量的键值对实参
    user_info['first_name'] = first
    user_info['last_name'] = last

    return user_info

user_profile = buil_profile('a', 'b', age=8, habby='bastball')
print(user_profile)

'''
'age': 8, 'habby': 'bastball', 'first_name': 'a', 'last_name': 'b'}
'''