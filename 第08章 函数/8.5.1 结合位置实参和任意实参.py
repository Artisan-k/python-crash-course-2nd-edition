
charts = ['A', 'B', 'C']

def do(size,*charts):
    print(f'There are {size} charts:')
    for c in charts:
        print(c)

do(2, 'a', 'c')

'''
There are 2 charts:
a
c
'''