def greet_users(names):
    for name in names:
        msg = f'Hello, {name.title()}!'
        print(msg)

usernames = ['hannah', 'ty', 'margot']
greet_users(usernames)

'''
Hello, Hannah!
Hello, Ty!
Hello, Margot!
'''