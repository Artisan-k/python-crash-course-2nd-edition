filename = '第10章 文件和异常/programing.txt'

with open(filename, 'a') as file_object:
    '''
    'a':附加模式，返回文件对象前不会清空文件
    '''
    file_object.write("I also love finding meaning in large datasets.\n")
    file_object.write("I love creating apps that can run in a browser.\n")
