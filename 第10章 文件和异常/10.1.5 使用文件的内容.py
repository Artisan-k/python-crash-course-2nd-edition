filename = '第10章 文件和异常/pi_digits.txt'

with open(filename) as file_object: 
    lines = file_object.readlines()

pi_string =  ''
for line in lines:
    pi_string += line.strip()  # 删除两边的空格

print(pi_string)
print(len(pi_string))
'''
3.141592653589793238462643383279
32
'''
