print("输入两个数字，然后两者相除。")
print("输入'q' 退出。")

while True:
  first_num = input("除数: ")
  if first_num == 'q':
    break

  second_num = input("被除数: ")
  if second_num == 'q':
    break

  try:
    answer= int(first_num) / int(second_num)
  except:
    print("You can't divide by 0!")
  else:
    print(answer) # 无异常，执行这个代码块


'''
输入两个数字，然后两者相除。
输入'q' 退出。
除数: 10
被除数: 5
2.0

除数: 10
被除数: 0
You can't divide by 0!

'''


