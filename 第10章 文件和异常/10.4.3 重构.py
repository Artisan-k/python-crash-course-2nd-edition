import json

def get_stored_username():
    filename = '第10章 文件和异常/username.json'
    try:
        with open(filename) as f:
            username = json.load(f)
    except FileNotFoundError:
        return None
    else:
        return username

def get_new_username():
    username = input("What's your name? ")
    filename = '第10章 文件和异常/username.json'
    with open(filename, 'w') as f:
        json.dump(username, f)
    
    return username

def greet_user():
    '''问候用户，并指出其名字。'''
    username = get_stored_username()
    if username:
        print(f"Welcome back, {username}!")
    else:
        username = get_new_username()
        print(f"We'll remember you when you come back, {username}!")
        

greet_user()
