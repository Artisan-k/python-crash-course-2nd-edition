import json

username = input("What's your name? ")
filename = '第10章 文件和异常/username.json'

# 存储
with open(filename, 'w') as f:
    json.dump(username, f)
    print(f"We'll remember you when you come back, {username}!")

# 读取
with open(filename) as f:
    your_username = json.load(f)
    print(f"Welcome back, {your_username}!")


'''
What's your name? Kevin
We'll remember you when you come back, Kevin!
Welcome back, Kevin!
'''