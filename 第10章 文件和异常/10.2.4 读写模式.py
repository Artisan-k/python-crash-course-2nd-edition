filename = '第10章 文件和异常/programing.txt'

with open(filename, 'r+') as file_object:
    '''
    'r+': 读写(追加)模式，文件不存在会抛出异常，不会自动创建
          其中，+:是追加模式 

    因为r+打开的文件，光标默认在开头，因此要把光标移动到最后。才可以追加写入,
    使用seek()方法移动光标：
    seek(offset, whence)方法:
     - offset: 偏移量
     - whence: 指定偏移的起始位置
        0-代表从文件开头开始算起
        1-代表从当前位置开始算起，
        2-代表从文件末尾算起
    '''

    # 写入
    file_object.seek(0,2) # 光标移动到文件末尾位置
    file_object.write("\nTest r+ mode.")

    # 读取
    file_object.seek(0,0) # 光标移动到文件起始位置
    contents = file_object.read()
    print(contents.rstrip())
