filename = '第10章 文件和异常/pi_digits.txt'

with open(filename) as file_object: 
    for line in file_object:
        print(line.rstrip()) # rstrip() 去掉每行末尾的看不见的换行符
