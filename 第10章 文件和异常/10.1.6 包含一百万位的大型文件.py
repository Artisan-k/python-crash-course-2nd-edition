filename = '第10章 文件和异常/pi_million_digits.txt'

with open(filename) as file_object: 
    lines = file_object.readlines()

pi_string =  ''
for line in lines:
    pi_string += line.strip()  

print(f'{pi_string[:52]}...') # 只打印小数点后50位
print(len(pi_string))
'''
3.14159265358979323846264338327950288419716939937510...
1000002
'''
