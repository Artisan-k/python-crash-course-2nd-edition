def count_words(filename):
    try:
        with open(filename, encoding='utf-8') as f:
            contents = f.read()
    except FileNotFoundError:
        print(f'Sorry, the file {filename} does not exist.')
    else:
        # 计算该文件大致包含多少个单词
        words = contents.split()
        num_words = len(words)
        print(f"The file  {filename} has about {num_words} words.")


filenames = ['第10章 文件和异常/alice.txt', '第10章 文件和异常/siddhartha.txt']
for filename in filenames:
    count_words(filename)


'''
The file  第10章 文件和异常/alice.txt has about 29465 words.
The file  第10章 文件和异常/siddhartha.txt has about 42172 words.
'''