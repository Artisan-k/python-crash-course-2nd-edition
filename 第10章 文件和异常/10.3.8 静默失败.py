def count_words(filename):
    try:
        with open(filename, encoding='utf-8') as f:
            contents = f.read()
    except FileNotFoundError:
        pass # pass 语句表示什么都不做，静默处理
    else:
        words = contents.split()
        num_words = len(words)
        print(f"The file  {filename} has about {num_words} words.")


filenames = ['第10章 文件和异常/alice-1.txt', '第10章 文件和异常/siddhartha.txt']
for filename in filenames:
    count_words(filename)


'''
The file  第10章 文件和异常/siddhartha.txt has about 42172 words.
'''