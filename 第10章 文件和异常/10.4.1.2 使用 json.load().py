from fileinput import filename
import json

filename = '第10章 文件和异常/numbers.json'

with open(filename) as f:
    numbers = json.load(f)

print(numbers)
'''
[2, 3, 5, 7, 11, 13]
'''