filename = '第10章 文件和异常/empty_file.txt'

with open(filename, 'w') as file_object:
    '''
     'w' 写入模式，
    文件不存在，自动创建， 
    若已存在，清空文件后再返回文件对象
    '''
    file_object.write("I love programing 2!")
