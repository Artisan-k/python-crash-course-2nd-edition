class Car:
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0 # 给属性默认值为0

    def get_descriptive_name(self):
        long_name = f'{self.year} {self.make} {self.model}'
        return long_name.title()

    def read_odometer(self):
        print(f'This car has {self.odometer_reading} miles on it.')
   
    def update_odometer(self, mileage):
        if mileage >= self.odometer_reading:
            self.odometer_reading = mileage
        else:
            print("You can't roll back an odometer!")

    def increment_odometer(self, miles):
        self.odometer_reading += miles


class ElectricCar(Car):
    def __init__(self, make, model, year):
        """初始化父类的属性"""
        super().__init__(make, model, year)
        '''初始化电动车的特有的属性'''
        self.battery_size = 75
    
    def describe_battery(self):
        print(f"This car has a {self.battery_size}-kwh battery.")

my_car = ElectricCar('audi', 'a4', '2019')
print(my_car.get_descriptive_name()) 
my_car.describe_battery()

'''
2019 Audi A4
This car has a 75-kwh battery.
'''