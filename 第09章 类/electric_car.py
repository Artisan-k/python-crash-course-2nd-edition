
from car import Car # 在一个模块中导入另一个模块

class ElectricCar(Car):
    def __init__(self, make, model, year):
        """初始化父类的属性"""
        super().__init__(make, model, year)
        '''初始化电动车的特有的属性'''
        self.battery = Battery()

    def fill_gas_tank(self):
        '''重写父类方法'''
        print("This car doesn't need a gas tank!")

class Battery:
    def __init__(self, battery_size=75):
        self.battery_size = battery_size

    def describe_battery(self):
        print(f"This car has a {self.battery_size}-kwh battery.")