class Dog: # 类名大写字母开头

    '''
    1. __init__ 方法，创建类实例时自动调用,
    2. 参数self必须排第一个, 并且自动传参
    '''
    def __init__(self, name, age): # __init__ 方法，创建类实例时自动调用
        self.name = name # 初始化属性
        self.age = age # 初始化属性

    def sit(self):
        print(f'{self.name} is now sitting.')

    def roll_over(self):
        print(f'{self.name} rolled over.')

    