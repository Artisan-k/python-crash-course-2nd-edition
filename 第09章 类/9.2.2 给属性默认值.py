class Car:
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0 # 给属性默认值为0
    

    def get_descriptive_name(self):
        long_name = f'{self.year} {self.make} {self.model}'
        return long_name.title()


    def read_odometer(self):
        print(f'This car has {self.odometer_reading} miles on it.')

my_car = Car('audi', 'a4', '2019')
print(my_car.get_descriptive_name()) 
my_car.read_odometer()

'''
2019 Audi A4
This car has 0 miles on it.
'''