from car import Car, ElectricCar # 打开 car 模块，导入多个类，类间使用逗号隔开

my_car = Car('audi', 'a4', '2019')
print(my_car.get_descriptive_name())

my_tesla = ElectricCar('tesla', 'roadster', '2019')
print(my_tesla.get_descriptive_name())


'''
2019 Audi A4
2019 Tesla Roadster
'''