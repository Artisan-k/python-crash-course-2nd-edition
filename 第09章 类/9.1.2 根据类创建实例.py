class Dog: # 类名大写字母开头

    '''
    1. __init__ 方法，创建类实例时自动调用,
    2. 参数self必须排第一个, 并且自动传参
    '''
    def __init__(self, name, age): # __init__ 方法，创建类实例时自动调用
        self.name = name # 属性
        self.age = age # 属性


    def sit(self):
        print(f'{self.name} is now sitting.')


    def roll_over(self):
        print(f'{self.name} rolled over.')


my_dog = Dog('Willie', 6)

print(my_dog.name)
print(my_dog.age)
my_dog.sit()
my_dog.roll_over()

'''
Willie
6
Willie is now sitting.
Willie rolled over.  
'''
