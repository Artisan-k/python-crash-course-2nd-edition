from car import Car # 打开 car 模块，并导入 Car 类

my_car = Car('audi', 'a4', '2019')
print(my_car.get_descriptive_name())
my_car.odometer_reading = 23
my_car.read_odometer() 

'''
2019 Audi A4
This car has 23 miles on it.
'''