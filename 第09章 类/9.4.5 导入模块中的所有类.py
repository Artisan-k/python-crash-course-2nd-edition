from car import * # 打开 car 模块，导入模块中的所有类，但是不推荐这种导入方式

my_car = Car('audi', 'a4', '2019')
print(my_car.get_descriptive_name())

my_tesla = ElectricCar('tesla', 'roadster', '2019')
print(my_tesla.get_descriptive_name())


'''
2019 Audi A4
2019 Tesla Roadster
'''