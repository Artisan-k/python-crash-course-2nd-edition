numbers = list(range(1,6))
print(numbers) # [1, 2, 3, 4, 5]

print("--------------------")

numbers = list(range(0,11,2)) # 第三个参数是指定步长
print(numbers) # [0, 2, 4, 6, 8, 10]

print("--------------------")

squares = []
for value in range(1,11):
   square = value ** 2
   squares.append(value ** 2) # 不使用临时变量 square
   # squares.append(square)
 
print(squares)  # [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

