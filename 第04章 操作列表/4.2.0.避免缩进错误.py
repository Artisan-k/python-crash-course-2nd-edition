# Python 根据缩来判断代码行与前一行代码的关系
magicians = ['alice','david','carolina']
for magician in magicians:
    print(f"{magician.title()}, that was a great trick!")
    print(f"I can't wait to see your next trick, {{magician.title()}}.\n") # 缩进的代码行是循环的一部分

print("Than you, everyone")

