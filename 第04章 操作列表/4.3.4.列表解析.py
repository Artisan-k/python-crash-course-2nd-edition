# 1.for value in range(1,11) 给 表达式 value ** 2 提供值
# 2.然后把 表达式 value ** 2 的结果值自动赋值给 列表 squares
squares = [value ** 2 for value in range(1,11)]
print(squares) # [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

