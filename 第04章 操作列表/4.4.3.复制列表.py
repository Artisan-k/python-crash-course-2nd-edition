chars = ["A", "B", "C"]
print("----使用切片拷贝---")
charsCopy = chars[:]
print(chars) # ['A', 'B', 'C']
print(charsCopy) # ['A', 'B', 'C']

print("----分别追加元素---")
chars.append("D")
charsCopy.append("F")
print(chars) # ['A', 'B', 'C', 'D']
print(charsCopy) # ['A', 'B', 'C', 'F'] # 切片拷贝后的列表是原列表的副本，是独立的