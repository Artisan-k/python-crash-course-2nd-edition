chars = ["A", "B", "C", "D", "E"]
print(chars[0:2]) # ['A', 'B'] # 到达第二个索引之前的元素后停止

print(chars[1:2]) # ['B']

print(chars[:2]) # ['A', 'B']

print(chars[2:]) # ['C', 'D', 'E']

print(chars[-2:]) # ['D', 'E'] # 负数索引，返回离尾部相应距离的元素，故返回尾部2个元素， 
