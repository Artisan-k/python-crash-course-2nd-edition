# Python 根据缩来判断代码行与前一行代码的关系
magicians = ['alice','david','carolina']
for magician in magicians:
    print(f"{magician.title()}, that was a great trick!")
print(f"I can't wait to see your next trick, {{magician.title()}}.\n") # 本应缩进，语法没错，是一个逻辑错误

print("Than you, everyone")

