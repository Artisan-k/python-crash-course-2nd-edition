pointer = (200, 50) # 定义元组：不可变的列表称为元组
print(pointer) # (200, 50)
print(pointer[0]) # 200
print(pointer[1]) # 50

# 不可变的列表称为元组，修改元组的操作是被禁止的
# pointer[0] = 200 # 抛出异常： 'tuple' object does not support item assignment

