favorite_languages = {
    "jen": "python",
    "sarah": "C",
    "edward":"ruby",
    "phil": "C#",
}
for name in favorite_languages.keys():
    print(name)

print("-------------------")

for name in sorted(favorite_languages.keys()) : # 升序排序
    print(name)

print("-------------------")


for name in favorite_languages.keys():
    print(name)

'''
jen
sarah
edward
phil
-------------------
edward
jen
phil
sarah
-------------------
jen
sarah
edward
phil
'''