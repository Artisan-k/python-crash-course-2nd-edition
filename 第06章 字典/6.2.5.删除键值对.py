alien = {'name':'alien', 'age': 18}
print(alien)
print(alien['name'])
print(alien['age'])

del alien['name'] # 删除键值对

# print(alien['name']) # 删除后访问，发送异常
print(alien['age'])