alien_0 = {'name':'jack', 'age': 10 }
alien_1 = {'name':'tom', 'age': 9 }
alien_2 = {'name':'alice', 'age': 25 }

aliens = [alien_0, alien_1, alien_2]
print(aliens) # [{'name': 'jack', 'age': 10}, {'name': 'tom', 'age': 9}, {'name': 'alice', 'age': 25}]

for alien in aliens:
    print(alien)
'''
{'name': 'jack', 'age': 10}
{'name': 'tom', 'age': 9}
{'name': 'alice', 'age': 25}
'''

print(len(aliens)) # 3



for alien in aliens:
    if(alien['name'] == 'alice'):
       alien['age'] += 1

for alien in aliens:
    print(alien)
'''
{'name': 'jack', 'age': 10}
{'name': 'tom', 'age': 9}
{'name': 'alice', 'age': 26}
'''