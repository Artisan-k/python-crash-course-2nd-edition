favorite_languages = {
    "jen": "python",
    "sarah": "C",
    "edward":"ruby",
    "phil": "python",
}
for language in favorite_languages.values(): # 注意：没有去重哦，输出两个 python
    print(language)

'''
python
C
ruby
python
'''

print('---------------')

favorite_languages = {
    "jen": "python",
    "sarah": "C",
    "edward":"ruby",
    "phil": "python",
}
for language in set(favorite_languages.values()): # # 使用 set去重
    print(language)

'''
python
C
ruby
'''