users = {
    'jack': {
            'name':'jack',
            'hobbys': ["篮球", "足球", "游泳"],
        },
    'tom':{
        'name':'jack',
        'hobbys': ["抓老鼠"],
    }
}

for name, info in users.items():
    print("-------------")
    print(f"name:{name}")
    print(f"bobbys:{info['hobbys']}")

'''
-------------
name:jack
bobbys:['篮球', '足球', '游泳']
-------------
name:tom
bobbys:['抓老鼠']
'''