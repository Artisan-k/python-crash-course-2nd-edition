user = {
    'name':'jack',
    'hobbys': ["篮球", "足球", "游泳"],
}

print(f"姓名:{user['name']}")
print(f"爱好:")
for hobby in user["hobbys"]:
    print(hobby)
'''
姓名:jack
爱好:
篮球
足球
游泳
'''