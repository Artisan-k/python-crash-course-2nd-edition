favorite_languages = {
    "jen": "python",
    "sarah": "C",
    "edward":"ruby",
    "phil": "C#",
}

print(f"Phil's favorite language is {favorite_languages['phil']}") # Phil's favorite language is C#