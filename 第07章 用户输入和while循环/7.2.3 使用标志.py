prompt = '\n告诉我一些事情，我将重复你的话'
prompt += "\n输入'quit' 退出程序。"

active = True # 这个变量称为标志（flag）
while active :
    message = input(prompt)
    if(message == 'quit'):
      active = False
    else:
      print(message)