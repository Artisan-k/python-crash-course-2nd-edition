
from timeit import repeat
from urllib import response


responses = {}
polling_actvie = True

while polling_actvie:
  name = input("\n你的名字: ")
  response = input("你的爱好? ")

  # 将回答存储在字典中
  responses[name] = response
  
  repeat = input('继续? yes/no: ')
  if repeat == 'no':
    polling_actvie  = False

# 显示结果
print('\nresponses:', responses)

'''
你的名字: kevin
你的爱好? 爬山
继续? yes/no: yes

你的名字: jack 
你的爱好? 滑雪
继续? yes/no: no

responses: {'kevin': '爬山', 'jack': '滑雪'}
'''
