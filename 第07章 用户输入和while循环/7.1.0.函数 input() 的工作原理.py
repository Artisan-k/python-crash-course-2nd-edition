# 函数 input() 让程序暂停，等待用户输入，第一个参数：向用户显示的提示
message = input("请输入:")
print(f"刚才你输入的是:{message}")

'''
请输入:hi
刚才你输入的是:hi
'''