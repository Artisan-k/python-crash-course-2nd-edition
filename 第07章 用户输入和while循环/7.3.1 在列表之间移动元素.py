
import numbers


list1 = [1,2,3,4,5]
list2 = []

while list1:
  number = list1.pop()
  list2.append(number)

print("list1:",list1) # list1: []
print("list2:",list2) # list2: [5, 4, 3, 2, 1]
