# 在提示后面加冒号(:)和一个空格，即将提示和用户输入分开

message = input("Please input your name: ")
print(f"Hello, {message}")

'''
Please input your name: kevin
Hello, kevin
'''