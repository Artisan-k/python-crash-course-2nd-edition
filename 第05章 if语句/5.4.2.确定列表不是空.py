chars = []
if chars: # 使用列表名作为条件表达式，列表至少存在一个元素时返回True
    for char in chars:
        print(char)
else:
    print("列表为空")

'''
列表为空
'''
print("-------------------")
chars = ["A","B","C"]
if chars: # 使用列表名作为条件表达式，列表至少存在一个元素时返回True
    for char in chars:
        print(char)
else:
    print("列表为空")

'''
A
B
C
'''