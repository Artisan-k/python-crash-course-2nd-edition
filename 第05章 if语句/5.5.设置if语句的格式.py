# PEP 8 建议： 条件测试的格式在比较运算符两边各添加一个空格
age = 8
if age == 8:
    print("推荐格式:age == 8")
    print("不推荐格式:age==8")

