from ast import For


chars1 = ["A","B","C"]
chars2 = ["B","C"]

for char in chars1:
    if char in chars2:
       print(f"同时存在{char}")

'''
同时存在B
同时存在C
'''