age = 18
print(age == 18) # True
print(age > 18) # False
print(age >= 18) # True
print(age < 18) # False
print(age <= 18) # True