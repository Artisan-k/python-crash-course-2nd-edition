chars = ['b','a', 'c', 'd']
chars.sort()
print(chars) # ['a', 'b', 'c', 'd']

# 反序
chars.sort(reverse=True)
print(chars) # ['d', 'c', 'b', 'a']