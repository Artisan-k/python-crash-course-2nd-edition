chars = ['b','a', 'c', 'd']

print(sorted(chars)) # ['a', 'b', 'c', 'd']
print(sorted(chars, reverse=True)) # ['d', 'c', 'b', 'a']
print(chars) # ['b', 'a', 'c', 'd']
