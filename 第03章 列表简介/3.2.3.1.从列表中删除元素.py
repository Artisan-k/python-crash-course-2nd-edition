motorycycles = ["honda","yamaha","suzuki"]
print(motorycycles) 

del motorycycles[0]
print(motorycycles)  # ['yamaha', 'suzuki']

del motorycycles[0]
print(motorycycles)  # ['suzuki']

del motorycycles[0]
print(motorycycles)  # []