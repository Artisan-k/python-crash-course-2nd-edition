motorycycles = ["honda","yamaha","suzuki"]
print(motorycycles) 

motorycycles.append("ducati") 
print(motorycycles)  # ['honda', 'yamaha', 'suzuki', 'ducati']

motorycycles = ["honda","yamaha","suzuki"]
motorycycles.insert(0,"ducati") 
print(motorycycles)  # ['ducati','honda', 'yamaha', 'suzuki']