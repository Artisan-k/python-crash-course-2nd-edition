python-crash-course-2nd-edition

Python 编程 从入门到实践 第二版 代码


# 安装 Python

## 检查安装

安装完Python，检查是否安装成功

```powershell
C:\Users\wei>python
Python 3.10.2 (tags/v3.10.2:a58ebcc, Jan 17 2022, 14:12:15) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> print("hello python")
hello python
>>> ctrl + z  > 回车 > 退出
```

# 运行

在 Visual Code 中新建文件 `hello_world.py`

```pyt
print("hello world")
```



## CMD 运行

打开CMD，切换到  `hello_world.py` 所在的目录：

```powershell
cd E:\Kzone\CodeLib\PythonCode\Book-Python-Crash-Course\Chapter-01>
```

使用 python 命令执行：

```powershell
E:\Kzone\CodeLib\PythonCode\Book-Python-Crash-Course\Chapter-01>python hello_world.py
hello world
```

或者

```powershell
E:\Kzone\CodeLib\PythonCode\Book-Python-Crash-Course\Chapter-01>py hello_world.py     
hello world
```



## Visual Code 运行

使用 Visual Code 打开文件  `hello_world.py` ，选择菜单【运行 > 启动调试 】，在配置下拉框中选择【Python 文件】

![1651912782605](Python-Crash-Course-2nd/images/1651912782605.png)

选择菜单【运行 > 启动调试 】或按快捷键 F5，输出如下结果：

```powshell
E:\Kzone\CodeLib\PythonCode\Book-Python-Crash-Course> e: && cd e:\Kzone\CodeLib\PythonCode\Book-Python-Crash-Course && cmd /C "C:\Users\wei\AppData\Local\Programs\Python\Python310\python.exe c:\Users\wei\.vscode\extensions\ms-python.python-2022.6.0\pythonFiles\lib\python\debugpy\launcher 10226 -- "e:\Kzone\CodeLib\PythonCode\Book-Python-Crash-Course\Chapter-01 起步\hello_world.py" "
hello world
```


