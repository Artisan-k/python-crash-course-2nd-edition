# 整个规划
要看最终版，到本书的资源地址下载源码

https://www.ituring.com.cn/book/2784 

打开chapter_14这个文件夹，输入如下命令启动游戏：

```shell
chapter_14>cd adding_play_button
chapter_14\adding_play_button>python alien_invasion.py
pygame 2.1.2 (SDL 2.0.18, Python 3.10.2)
Hello from the pygame community. https://www.pygame.org/contribute.html

按  alt + f4 退出
```
游戏界面：

<img src="12.1 规划项目/images/playstart.png" alt="playstart" style="zoom:80%;" /> 

左右键移动，空格键射击

<img src="12.1 规划项目/images/playing.png" alt="playing" style="zoom:80%;" />

# 第一阶段

开发的第一阶段，创建一艘飞船，它可以左右移动，并能在用户按下空格键时开火